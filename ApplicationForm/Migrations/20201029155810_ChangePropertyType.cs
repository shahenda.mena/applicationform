﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApplicationForm.Migrations
{
    public partial class ChangePropertyType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Gender",
                table: "Candidates",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Gender",
                table: "Candidates",
                type: "bit",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
