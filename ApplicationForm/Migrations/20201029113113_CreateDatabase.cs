﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ApplicationForm.Migrations
{
    public partial class CreateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Candidates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<bool>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Area = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    ProfileImage = table.Column<byte[]>(nullable: false),
                    CV = table.Column<byte[]>(nullable: false),
                    ApplicationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Candidates", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Candidates");
        }
    }
}
