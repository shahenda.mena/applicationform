﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ApplicationForm.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationForm.Controllers
{
    public class CandidateController : Controller
    {
        private readonly CandidateDbContext _context;
        public CandidateController(CandidateDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Candidate candidate, IFormFile cvFile, IFormFile profileFile)
        {
            if (ModelState.IsValid)
            {
                using (var ms = new MemoryStream())
                {
                    profileFile.CopyTo(ms);
                    candidate.ProfileImage = ms.ToArray();
                }

                using (var target = new MemoryStream())
                {
                    cvFile.CopyTo(target);
                    candidate.CV = target.ToArray();
                }

                candidate.ApplicationDate = DateTime.Now;
                _context.Candidates.Add(candidate);
                _context.SaveChanges();

                return View("submit");
            }

            return View("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
